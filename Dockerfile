FROM alpine:3.18

LABEL maintainer="Dmitriy Kuts me@exileed.com"

ARG ANSIBLE_VERSION
ARG ANSIBLE_LINT_VERSION
#ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ENV PYTHONUNBUFFERED=1


RUN set -xe \
    && apk add --no-cache --progress python3  openssl \
        ca-certificates git openssh sshpass py3-pip \
    && apk --update add --virtual build-dependencies \
        python3-dev libffi-dev openssl-dev build-base \
    \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir ansible==${ANSIBLE_VERSION} boto3 cryptography \
      ansible-lint==${ANSIBLE_LINT_VERSION} \
    \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/*

CMD ["ansible", "--version"]